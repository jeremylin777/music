import { createStore, combineReducers, applyMiddleware } from "redux";
import { user } from "./reducers/user";
import thunk from "redux-thunk";
const reducer = combineReducers({ user });

const store = createStore(reducer, applyMiddleware(thunk));

export default store;
