import { SET_TOKEN, SET_USER, SET_COOKIE } from "../actionTypes";
const initState = {
  profile: {},
  token: "",
  cookie: "",
};
export const user = (state = initState, action) => {
  let { type, payload } = action;
  switch (type) {
    case SET_COOKIE:
      return { ...state, cookie: payload };
      break;
    case SET_USER:
      return { ...state, profile: payload };
      break;
    case SET_TOKEN:
      return { ...state, token: payload };
      break;
    default:
      return state;
  }
};
