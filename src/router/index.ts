import Home from "../view/home";
import Yuncun from "../view/yuncun"
import Guanzhu from "../view/guanzhu";
import Wode from "../view/wode";
import Boke from "../view/boke";
import Login from "../view/login"
import Hot from "../view/hot";
import {
  AaOutline,
  AddCircleOutline, AddSquareOutline, CalendarOutline, CouponOutline
} from 'antd-mobile-icons'

interface IRoutes {
  path: string,
  element:any,
  title: string,
  icon?:any,
  hidden?:boolean
}

export const routes:IRoutes[] = [
  {
    path: "/",
    icon: AaOutline ,
    element: Home,
    title: '发现'
  },
   {
    path: "/boke",
    icon: AddCircleOutline ,
    element: Boke,
    title: '播客'
  },
    {
    path: "/wode",
    icon: AddSquareOutline ,
    element: Wode,
    title: '我的'
  },
     {
    path: "/guanzhu",
    icon: CalendarOutline ,
    element: Guanzhu,
    title: '关注'
  },
  {
    path: "/yuncun",
    icon: CouponOutline ,
    element: Yuncun,
    title: '云村'
  },
  {
    path: "/login",
    icon: AaOutline ,
    element: Login,
    title: '请登录',
    hidden:true
  },
   {
    path: "/hot/:name",
    icon: '' ,
    element: Hot,
    title: '热门搜索',
    hidden:true
  },
]