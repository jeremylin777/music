import React from "react";
import { routes } from "./router";
import {
  Routes,
  Route,
  NavLink,
  useNavigate,
  useParams,
  useSearchParams,
} from "react-router-dom";
import "./app.css";

export default function App() {
  const navigate = useNavigate();
  return (
    <div>
      <Routes>
        {routes.map((item) => (
          <Route
            key={item.path}
            path={item.path}
            element={<item.element navigate={navigate} />}
          ></Route>
        ))}
      </Routes>
    </div>
  );
}
