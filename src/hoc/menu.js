import Memu from "../components/menu";
function menu(Com) {
  const Menu = (props) => {
    return (
      <>
        <Com {...props} />
        <Memu />
      </>
    );
  };

  return Menu;
}

export default menu;
