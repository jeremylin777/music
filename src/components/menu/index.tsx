import React from "react";
import { routes } from "../../router";
import { NavLink } from "react-router-dom";
export default function Menu() {
  return (
    <ul className="app_menu">
      {routes
        .filter((item) => !item.hidden)
        .map((item) => (
          <li key={item.path}>
            <NavLink to={item.path}>
              <item.icon style={{ fontSize: 30 }} />
              <p>{item.title}</p>
            </NavLink>
          </li>
        ))}
    </ul>
  );
}
