import request from "../../utils/request";

export interface ISearch {
  type?: 'mobile',
  keywords:string
}

export const getSearchList = (params: ISearch) => request.get("/search/suggest",{params})
// 搜索
export interface ISearchFn{
  keywords:string,
  limit?:number,
  offset?:number
}
export const search=(params:ISearchFn)=>request.get('/search',{params})