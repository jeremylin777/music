import React, { useState, useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";
import menu from "../../hoc/menu";
import style from "../home/style.module.scss";
import { LeftOutline } from "antd-mobile-icons";
import { Input } from "antd-mobile";
import styleHot from "./style.module.scss";
import { getSearchList, ISearchFn,search } from "./api";
//截流
import { throttle } from "../../utils";

function Hot() {
  const navigate = useNavigate();
  const params = useParams();
  // 搜索列表
  const [searchList, setSearchList] = useState([]);
  // console.log(params, "params");
  const [value, setValue] = useState(params.name);
  // 获取搜索数据

  const get_serach_list = async (value: string) => {
    if (value != "") {
      let res = await getSearchList({
        keywords: value || "",
        type: "mobile",
      });
      console.log(res, "search");

      if (res.data.code == 200) {
        // 不能返回其它类型 一定返回数组
        setSearchList(res.data.result.allMatch || []);
      }
    }
  };

  return (
    <div style={{ height: "100vh" }}>
      <div className={style.top}>
        <LeftOutline onClick={() => navigate("/")} />
        <Input
          style={{ paddingLeft: 22, fontSize: 12 }}
          value={value}
          onChange={throttle((value: string) => {
            get_serach_list(value);
            setValue(value);
          }, 2000)}
        ></Input>
        <span style={{ fontSize: 20 }}>搜索</span>
      </div>

      <div>
        <div>搜索列表</div>
        <ul>
          {searchList.map((item: any, i: number) => (
            <li
              onClick={() => {
                console.log(item.keyword, "kewwrold");
                setValue(item.keyword);
                setSearchList([]);
              }}
              style={{ padding: 10 }}
              key={i}
            >
              {item.keyword}{" "}
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
}
export default menu(Hot);
