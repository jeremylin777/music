import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Form, Button, Input, Toast } from "antd-mobile";
import { login, ILogin } from "./api";
import { SET_COOKIE, SET_TOKEN, SET_USER } from "../../store/actionTypes";
interface IProps {
  n: number;
  onLogin: (values: ILogin) => void;
}
function Login(props: IProps) {
  const onFinish = (values: ILogin) => {
    props.onLogin(values);
  };
  return (
    <div style={{ height: "100vh", background: "#999" }}>
      <Form
        onFinish={onFinish}
        footer={
          <Button block type="submit" color="primary" size="large">
            登陆
          </Button>
        }
      >
        <Form.Item
          name="phone"
          label="手机号"
          rules={[{ required: true, message: "手机号码不为空" }]}
        >
          <Input placeholder="请输入手机号" />
        </Form.Item>
        <Form.Item
          name="password"
          label="密码"
          rules={[{ required: true, message: "密码不为空" }]}
        >
          <Input type="password" placeholder="请输入密码密码" />
        </Form.Item>
      </Form>
    </div>
  );
}

function mapStateToProps() {
  return { n: 10 };
}

function mapDispatchToProps(dispatch: any, onwerProps: any) {
  return {
    onLogin: async (values: ILogin) => {
      let res = await login(values);
      // console.log(res, "reslogin");
      if (res.data.code == 200) {
        Toast.show({
          icon: "success",
          content: "登陆成功",
        });
        setTimeout(() => {
          onwerProps.navigate("/");
        }, 2000);
        dispatch({ type: SET_COOKIE, payload: res.data.cookie });
        dispatch({ type: SET_USER, payload: res.data.profile });
        dispatch({ type: SET_TOKEN, payload: res.data.token });
      } else {
        Toast.show({
          icon: "faile",
          content: "用户名或密码错误",
        });
      }
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
