import request from "../../utils/request";

export interface ILogin {
  phone: string;
  password: string;
}
export const login = (params: ILogin) =>
  request.get("/login/cellphone", { params });
