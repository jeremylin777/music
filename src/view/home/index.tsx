import React, { useState, useEffect } from "react";
import { Swiper, Popup, Button } from "antd-mobile";
import menu from "../../hoc/menu";
import { getBanner, getSearchList } from "./api";
import style from "./style.module.scss";
import { AppstoreOutline, AudioOutline } from "antd-mobile-icons";
import { useNavigate } from "react-router-dom";

interface ISwiper {
  pic: string;
  typeTitle: string;
  targetId: number;
  [propName: string]: any;
}
function Home() {
  const navigate = useNavigate();
  const [visible, setVisible] = useState<boolean>(false);
  const [bannerList, setBannerList] = useState<ISwiper[]>([]);
  const [tabs, setTabs] = useState<any[]>([]);
  const [index, setIndex] = useState<number>(0);
  const get_banner = async () => {
    let res = await getBanner(1);
    console.log(res, "banner");
    setBannerList(res.data.banners);
  };

  const get_search = async () => {
    let res = await getSearchList();
    console.log(res, "search");
    setTabs(res.data.result.hots);
  };
  useEffect(() => {
    get_banner();
    get_search();
  }, []);

  useEffect(() => {
    let time = setInterval(() => {
      if (index == tabs.length - 1) {
        setIndex(0);
      } else {
        setIndex(index + 1);
      }
    }, 10000);
    return () => {
      // 卸载的清除器
      // console.log(time, "time");
      clearInterval(time);
    };
  }, [index]);
  return (
    <div style={{ height: "100vh" }}>
      {tabs.length && (
        <div className={style.top}>
          <AppstoreOutline onClick={() => setVisible(true)} />
          <div
            onClick={() => {
              navigate("/hot/" + tabs[index].first);
            }}
            className="middle"
          >
            {tabs[index].first}
          </div>
          <AudioOutline />
        </div>
      )}
      <Swiper style={{ "--border-radius": "8px" }}>
        {bannerList.map((item) => (
          <Swiper.Item key={item.pic}>
            <img style={{ width: "100%" }} src={item.pic} alt="" />
          </Swiper.Item>
        ))}
      </Swiper>

      <Popup
        visible={visible}
        onMaskClick={() => {
          setVisible(false);
        }}
        position="left"
        bodyStyle={{ height: "100vh", width: "40vw" }}
      >
        <div>
          {" "}
          <span onClick={() => setVisible(false)}>X</span>
        </div>
        <Button
          onClick={() => {
            navigate("/login");
          }}
        >
          去登陆
        </Button>
      </Popup>
    </div>
  );
}

export default menu(Home);
