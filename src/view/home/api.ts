import request from "../../utils/request";

export const getBanner = (type?: number) => request.get("/banner", { params: { type } })

export const getSearchList = () => request.get("/search/hot")